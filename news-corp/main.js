// packages
const sqlite3 = require('sqlite3');
const https = require('https');
const express = require('express');

const database = new sqlite3.Database(':memory:', error => {
	if (!error) {
		database.serialize(() => {
			database.run(`CREATE TABLE Reference (
				ArticleId TEXT PRIMARY KEY,
				Title TEXT,
				Free BOOL,
				Standfirst TEXT,
				Authors TEXT,
				LiveDate DATETIME,
				UpdatedDate DATETIME,
				OriginalSource TEXT,
				Section TEXT,
				InDt BOOL
			)`);
		});
	}
});

https.get('https://resourcesssl.newscdn.com.au/cs/dna_apps/testing/testData.json', response => {
	let data = '';

	response.setEncoding('utf8')
		.on('data', buffer => {
			data += buffer;
		})
		.on('end', () => {
			try {
				const content = JSON.parse(data).content;

				if (content) {
					const references = content.references;

					if (references) {
						const keys = Object.keys(references);
						const keyCount = keys.length;

						for (let i = 0; i < keyCount; i++) {
							const reference = references[keys[i]];

							database.serialize(() => {
								database.run(`INSERT INTO Reference VALUES (
									'${reference.id}',
									'${reference.headline.default}',
									${reference.accessType.toLowerCase() === 'premium'},
									'${reference.standfirst.default}',
									"${reference.authors && reference.authors[0] ? reference.authors.map(author => author.name).join(',') : ''}",
									'${reference.date.live}',
									'${reference.date.updated}',
									'${reference.rightsMetadata.originatedSource}',
									'${reference.target.sections && reference.target.sections[0] ? reference.target.sections[0].id : ''}',
									${reference.domains ? reference.domains.find((index, domain) => domain.toLowerCase() === 'dailytelegraph.com.au') : false}
								)`);
							});
						}
					}
				}
			} catch (exception) {
				// SyntaxError exception
			}
		});
});

const app = express();

app.set('json spaces', 3);

app.listen(8080, () => {
	console.log('Express: http://localhost:8080');
});

app.get('/', (request, response) => {
	database.serialize(() => {
		const references = [];

		database.each('SELECT * FROM Reference', (error, reference) => {
			references.push({
				articleId : reference.ArticleId,
				title : reference.Title,
				free : Boolean(reference.Free),
				standfirst : reference.Standfirst,
				authors : reference.Authors.split(','),
				date : {
					liveDate : reference.LiveDate,
					dateUpdated : reference.UpdatedDate
				},
				originalSource : reference.OriginalSource,
				section : reference.Section,
				inDt : Boolean(reference.InDt)
			});
		}, (error, referenceCount) => {
			if (!error) {
				response.json(references);
			}
		});
	});
});